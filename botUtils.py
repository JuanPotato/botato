# -*- coding: utf-8 -*-
from __future__ import unicode_literals
__author__ = 'luckydonald'
import sys
import random

if sys.version < '3': # python 2.7
    text_type = unicode
    binary_type = str
    native_type = binary_type
    unicode_type = text_type  # because I can't remember to use text_type
    long_int = long
    def to_native(x):
        return to_binary(x)
else: # python 3
    text_type = str
    binary_type = bytes
    native_type = text_type
    unicode_type = text_type
    long_int = int
    def to_native(x):
        return to_unicode(x)

def to_binary(x):
    if isinstance(x, text_type):
        return x.encode("utf-8")
    else:
        return x

def to_unicode(x):
    if isinstance(x, binary_type):
        return x.decode("utf-8")
    else:
        return x

def ranString(length=16):
	return ''.join(random.choice('0123456789abcdefghjiklmnopqrstuvwxyz') for i in range(length))

def downloadImg(url, dir = "", localFileName = None):
	def url2name(url):
		return basename(urlsplit(url)[2])
	def ranString(length=16):
		return ''.join(random.choice('0123456789abcdefghjiklmnopqrstuvwxyz') for i in range(length))
	localName = url2name(url)
	req = urllib2.Request(url)
	r = urllib2.urlopen(req)
	if r.info().has_key('Content-Disposition'):
		localName = r.info()['Content-Disposition'].split('filename=')[1]
		if localName[0] == '"' or localName[0] == "'":
			localName = localName[1:-1]
	elif r.url != url: 
		localName = url2name(r.url)
	if localFileName: 
		localName = localFileName
	if "." in localName: 
		localName = "{}{}.{}".format(dir, ranString(), localName.split(".")[len(localName.split("."))-1])
	f = open(localName, 'wb')
	f.write(r.read())
	f.close()
	return localName