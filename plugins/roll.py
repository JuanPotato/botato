#!/usr/bin/python
# -*- coding: utf-8 -*-

import random


def function(sender, msg, matches, peer):
    if len(matches) == 1:
        return str(random.randrange(1, 6))
    elif len(matches) == 2:
        times = int(matches[1])
        times = 100 if times > 100 else times

        message = ", ".join(str(random.randrange(1, 6)) for i in range(times))
        return message
    elif len(matches) == 3:
        sides = int(matches[2])
        times = int(matches[1])
        times = 100 if times > 100 else times

        message = ", ".join(str(random.randrange(1, sides)) for i in range(times))
        return message

plugin = {
    'name': "Roll",
    'tag': "roll",
    'patterns': ["^/(roll)$", "^/(roll) (\d+)$", "^/(roll) (\d+)\s*d\s*(\d+)$"],
    'function': function,
    'elevated': False,
    'usage': "/roll (times) or /roll (times)d(sides)",
    'desc': "Receive information about yourself (mainly ID)"
    }
