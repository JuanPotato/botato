#!/usr/bin/python
# -*- coding: utf-8 -*-


def function(sender, msg, matches, peer):
    message = """A telegram bot written in python
    Not based on any bot
    Not using official bot api
    Made by (at)awkward_potato
    Running on a raspberry pi 2
    A Botato (part potato and part bot)"""
    return message

plugin = {
    'name': "About",
    'tag': "about",
    'patterns': ["^/about$"],
    'function': function,
    'elevated': False,
    'usage': "/about",
    'desc': "Shows the about text"
    }
