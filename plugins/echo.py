#!/usr/bin/python
# -*- coding: utf-8 -*-


def function(sender, msg, matches, peer):
    obfuscate = str.maketrans("ABCEIJKMHOPSTXYaceijosxy!", "АВСЕІЈКМНОРЅТХҮасеіјоѕхуǃ")

    message = matches[0].translate(obfuscate)
    sender.send_msg(peer, message)


plugin = {
    'name': "Echo",
    'tag': "echo",
    'patterns': ["^/echo (.*?)$"],
    'function': function,
    'elevated': False,
    'usage': "/echo <text>",
    'desc': "Repeats text"
    }
