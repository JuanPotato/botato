#!/usr/bin/python
# -*- coding: utf-8 -*-


def function(sender, msg, matches, peer):
    return "I love you too {}".format(msg["from"]["first_name"])

plugin = {
    'name': "Love",
    'tag': "",
    'patterns': ["^I? ?love (you)?,? Botato$"],
    'function': function,
    'elevated': False,
    'usage': "",
    'desc': ""
    }
