#!/usr/bin/python
# -*- coding: utf-8 -*-

import requests


def get_weather(city):
    payload = {
        'q': 'select * from weather.forecast where woeid in (select woeid from geo.places(1) where text="{}") AND u="c"'.format(city),
        'format': "json",
        'env': "store://datatables.org/alltableswithkeys"
    }

    try:
        data = requests.get("https://query.yahooapis.com/v1/public/yql", params=payload).json()
        data = data["query"]["results"]["channel"]
        cityName = "{}{}".format(data["location"]["city"], ", " + data["location"]["country"])
        tempInC = round(int(data["item"]["condition"]["temp"]), 2)
        tempInF = round((1.8 * tempInC) + 32, 2)
        condition = data["item"]["condition"]["text"]
        return "The weather in \"{}\" is currently {} °C ({} °F) and conditions are {}".format(cityName, tempInC, tempInF, condition)
    except:
        return "Could not find weather for that location."


def function(sender, msg, matches, peer):
    return get_weather(matches[0])

plugin = {
    'name': "Weather",
    'tag': "weather",
    'patterns': ["^/weather (.+?)$"],
    'function': function,
    'elevated': False,
    'usage': "/weather <city>",
    'desc': "Returns the weather of a city"
    }
