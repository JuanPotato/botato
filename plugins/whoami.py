#!/usr/bin/python
# -*- coding: utf-8 -*-


def getName(peer):
    return peer["print_name"].replace("_", " ")


def function(sender, msg, matches, peer):
    template = 'You, "{}", have an id of ({}) and are sending a message to "{}" ({})'
    return template.format(getName(msg["from"]), msg["from"]["id"], getName(msg["to"]), msg["to"]["id"])

plugin = {
    'name': "WhoAmI",
    'tag': "whoami",
    'patterns': ["^/whoami$", "^/ping$"],
    'function': function,
    'elevated': False,
    'usage': "/whoami",
    'desc': "Receive information about yourself (mainly ID)"
    }
