#!/usr/bin/python
# -*- coding: utf-8 -*-

import requests
import random
import base64


def function(sender, msg, matches, peer):
    key = 'SkC5RNEF77JYsGU2zR70KaeCrEeuK5Rk1lTd/0yXiVE'
    auth = 'Basic %s' % base64.b64encode((':%s' % key).encode("utf-8")).decode("utf-8")[:-1] + "="

    header = {'Authorization': auth}

    url = 'https://api.datamarket.azure.com/Data.ashx/Bing/Search/Image'

    payload = {
        'Query': "'" + matches[0] + "'",
        '$top': 10,
        '$format': 'json'
        }
    request = requests.get(url, params=payload, headers=header).json()

    try:
        if len(request["d"]["results"]) == 0:
            return "Could not find any images for that search."
        else:
            url = random.choice(request["d"]["results"])["Thumbnail"]["MediaUrl"]
            sender.send_imgUrl(peer, url, "")
    except:
        return "An error occured and I could not find any images for that search."

plugin = {
    'name': "Image",
    'tag': "img",
    'patterns': ["^/i(?:ma?ge?s?)? (.*?)$"],
    'function': function,
    'elevated': False,
    'usage': "/img <query>",
    'desc': "Returns an image using google images"
    }
