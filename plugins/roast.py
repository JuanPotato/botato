#!/usr/bin/python
# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
import requests


def function(sender, msg, matches, peer):
    try:
        a = requests.get("http://www.insultgenerator.org/")
        soup = BeautifulSoup(a.text)
        return soup.find("div", {"class": "wrap"}).text.strip()
    except:
        return "An error occured, try again later"

plugin = {
    'name': "Roast me",
    'tag': "roastme",
    'patterns': ["^/roastme$"],
    'function': function,
    'elevated': False,
    'usage': "/roastme",
    'desc': "Insults you"
    }
