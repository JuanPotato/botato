#!/usr/bin/python
# -*- coding: utf-8 -*-

import requests
import datetime
import time


def get_time(city):
    payload = {
        'address': city,
        'key': 'AIzaSyC2CoZgOXV118jnKvFwSUUfgkbZHtsCdTU'
    }

    try:
        data = requests.get("https://maps.googleapis.com/maps/api/geocode/json", params=payload).json()
        city_name = data["results"][0]["formatted_address"]

        data = data["results"][0]["geometry"]["location"]
        lat = str(data["lat"])
        long = str(data["lng"])

        now_time = int(time.time())

        payload = {
            'location': ",".join([lat, long]),
            'timestamp': now_time,
            'key': 'AIzaSyC2CoZgOXV118jnKvFwSUUfgkbZHtsCdTU'
            }

        data = requests.get("https://maps.googleapis.com/maps/api/timezone/json", params=payload).json()

        timestamp = now_time + data["rawOffset"] + data["dstOffset"]
        formatted_time = datetime.datetime.utcfromtimestamp(timestamp).strftime('%Y-%m-%d %I:%M %p')
        zone = "GMT%+d" % (data["rawOffset"] / 3600)
        zone = zone + " DST" if data["dstOffset"] else zone
        return "{}\n{}\n{}".format(city_name, formatted_time, zone)
    except:
        return "Could not find the time for that location."


def function(sender, msg, matches, peer):
    return get_time(matches[0])

plugin = {
    'name': "Time",
    'tag': "time",
    'patterns': ["^/time (.+?)$"],
    'function': function,
    'elevated': False,
    'usage': "/time <city>",
    'desc': "Returns the time of a city"
    }
