# !/usr/bin/python
# -*- coding: utf-8 -*-


def function(sender, msg, matches, peer):
    return "¯\\_(ツ)_/¯"

plugin = {
    'name': "Shrug",
    'tag': "shrug",
    'patterns': ["^/shrug$"],
    'function': function,
    'elevated': False, 
    'usage' : "/shrug",
    'desc' : "Shrugs"
    }
