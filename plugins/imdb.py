#!/usr/bin/python
# -*- coding: utf-8 -*-

import requests


def get_imdb(search):
    payload = {'t': search}

    try:
        data = requests.get("http://www.omdbapi.com", params=payload).json()
        
        title = data["Title"] + " (" + data["Year"] + ") | " + data["imdbRating"] + "/10"
        info = " | ".join([data["Rated"], data["Runtime"], data["Genre"]])
        plot = data["Plot"]
        link = "http://imdb.com/title/" + data["imdbID"]

        return "\n".join([title, info, plot, link])
    except:
        return "Could not find info for that search."


def function(sender, msg, matches, peer):
    return get_imdb(matches[0])

plugin = {
    'name': "Imdb",
    'tag': "imdb",
    'patterns': ["^/imdb (.+?)$"],
    'function': function,
    'elevated': False,
    'usage': "/imdb <search>",
    'desc': "Returns the Imdb info for a given film or tv series"
    }
