#!/usr/bin/python
# -*- coding: utf-8 -*-

import requests


def function(sender, msg, matches, peer):
    req = requests.get('http://catfacts-api.appspot.com/api/facts').json()
    if req["success"] == "true":
        return req["facts"][0]
    else:
        return "Oops! An error occurred, try again later."

plugin = {
    'name': "Cat Facts",
    'tag': "catfact",
    'patterns': ["^/catfact$"],
    'function': function,
    'elevated': False,
    'usage': "/catfact",
    'desc': "Get a cat fact"
    }
