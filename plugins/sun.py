#!/usr/bin/python
# -*- coding: utf-8 -*-

import requests


def get_sun(city):
    payload = {
        'q': 'select astronomy, location from weather.forecast where woeid in (select woeid from geo.places(1) where text="{}")'.format(city),
        'format': "json",
        'env': "store://datatables.org/alltableswithkeys"
        }

    try:
        data = requests.get("https://query.yahooapis.com/v1/public/yql", params=payload).json()
        data = data["query"]["results"]["channel"]

        city_name = "{}{}".format(data["location"]["city"], ", " + data["location"]["country"])
        sunrise = data["astronomy"]["sunrise"]
        sunset = data["astronomy"]["sunset"]
        return "{}\nSunrise: {}\nSunset: {}".format(city_name, sunrise, sunset)
    except:
        return "Could not find weather for that location."


def function(sender, mag, matches, peer):
    return get_sun(matches[0])

plugin = {
    'name': "Sun",
    'tag': "sun",
    'patterns': ["^/sun(?:set|rise)? (.*?)$"],
    'function': function,
    'elevated': False,
    'usage': "/sun <city>",
    'desc': "Returns the sunset and sunrise times of a city"
    }
