#!/usr/bin/python
# -*- coding: utf-8 -*-

from PIL import ImageDraw, ImageFont, Image
import random


def function(sender, msg, matches, peer):
    def ranString(length=16):
        return ''.join(random.choice('0123456789abcdefghjiklmnopqrstuvwxyz') for i in range(length))
    palette = ["#000BAB", "#40E0D0", "#DC143C", "#4B0082", "#00DD00", "#00DDDD", "#DD0000", "#DD00FF", "#DDDD00", "#0112fe", "#ff4e00", "#741478", "#01870e"]
    im = Image.open("doge.jpg")
    texts = matches[0].split("/")
    draw = ImageDraw.Draw(im)
    font = ImageFont.truetype("dogeFont.ttf", 35)

    def typeText(text, color="#ff00ff"):
        size = font.getsize(text)
        x = random.randint(10, (500 - size[0] - 10))
        y = random.randint(10, (375 - size[1] - 10))
        draw.text((x, y), text, font=font, fill=color)

    a = 0
    for t in texts:
        if a >= len(palette):
            a = 0
        typeText(t, palette[a])
        a = a + 1

    name = "./tmp/" + sender.ranString() + ".jpg"
    im.save(name, "JPEG")
    sender.send_photo(peer, name, "")

plugin = {
    'name': "Dogify",
    'tag': "",
    'patterns': [],
    'function': function,
    'elevated': True,
    'usage': "/dogify words/or phrases seperated/by/dashes",
    'desc': "Makes a doge meme from given phrases"
    }
