#!/usr/bin/python
# -*- coding: utf-8 -*-


def function(sender, msg, matches, peer):
    try:
        sender.send_gifUrl(peer, "http://thecatapi.com/api/images/get?api_key=MjYwMTg&type=gif")
    except:
        return "An error occured, try again later"

plugin = {
    'name': "Catgif",
    'tag': "catgif",
    'patterns': ["^/(catgif)$"],
    'function': function,
    'elevated': False,
    'usage': "/catgif",
    'desc': "Returns a random cat gif"
    }
