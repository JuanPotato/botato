#!/usr/bin/python
# -*- coding: utf-8 -*-


def function(sender, msg, matches, peer):
    try:
        sender.send_imgUrl(peer, "http://thecatapi.com/api/images/get?api_key=MjYwMTg&type=png,jpg", "")
    except:
        return "An error occured, try again later"

plugin = {
    'name': "Cat",
    'tag': "cat",
    'patterns': ["^/(cat)$"],
    'function': function,
    'elevated': False,
    'usage': "/cat",
    'desc': "Returns a random cat image"
    }
