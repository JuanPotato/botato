#!/usr/bin/python
# -*- coding: utf-8 -*-

import random
import requests


def function(sender, msg, matches, peer):
    try:
        a = requests.get("http://xkcd.com/info.0.json").json()
        ran = random.randint(1, a["num"])
        a = requests.get("http://xkcd.com/{}/info.0.json".format(ran)).json()

        sender.send_imgUrl(peer, a["img"], '"{}"\n\n{}'.format(a["title"], a["alt"]))
    except:
        return "An error occured, try again later"

plugin = {
    'name': "Xkcd",
    'tag': "xkcd",
    'patterns': ["^/xkcd$"],
    'function': function,
    'elevated': False,
    'usage': "/xkcd",
    'desc': "Returns a random xkcd comic"
    }
