#!/usr/bin/python
# -*- coding: utf-8 -*-

import requests
import html as h


def function(sender, msg, matches, peer):
    payload = {'term': matches[0]}
    r = requests.get("http://api.urbandictionary.com/v0/define", params=payload).json()

    if r["result_type"] == "no_results":
        return "{} is not defined.".format(matches[0])
    else:
        example = h.unescape(r["list"][0]["example"])
        meaning = h.unescape(r["list"][0]["definition"])
        return "{}\n\nDefinition: {}\n\nExample: {}".format(matches[0], meaning, example)

plugin = {
    'name': "Urban Dictionary",
    'tag': "ud",
    'patterns': ["^/ud (.*?)$"],
    'function': function,
    'elevated': False,
    'usage': "/ud <term>",
    'desc': "Defines a term using Urban Dictionary"
    }
