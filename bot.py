#!/usr/bin/python
# -*- coding: utf-8 -*-

from botUtils import text_type, binary_type
from urllib.request import urlretrieve
from os.path import realpath, dirname
from botUtils import to_unicode as u
from botUtils import to_native as n
from botUtils import to_binary as b
from botUtils import ranString
from BotSettings import settings
from os import listdir
import subprocess
import threading
import socket
import json
import time
import re
import os


class Sender():
    _ANSWER_SYNTAX = b("ANSWER ")
    _LINE_BREAK = b("\n")
    default_answer_timeout = 1.0  # how long it should wait for a answer. DANGER: if set to None it will block!

    def __init__(self, host='', port=4458):
        self.s = None
        self.host = host
        self.port = port

    def send_imgUrl(self, user, url, caption, reply_id=None):
        image = urlretrieve(url, "./tmp/{}.jpg".format(ranString()))[0]
        return self.send_photo(user, image, caption, reply_id)

    def send_gifUrl(self, user, url, reply_id=None):
        file = urlretrieve(url, "./tmp/{}.gif".format(ranString()))[0]
        return self.send_document(user, file, reply_id)

    #SENDING MESSAGES
    def send_msg(self, user, text, reply_id=None, enable_preview=None):
        text = '"{}"'.format(text.replace("\\", "\\\\").replace("\"", "\\\"")).replace("\n", "\\n")
        try:
            return self.send("msg", ["{}#{}".format(user["type"], user["id"]), text], reply_id=reply_id, enable_preview=enable_preview)
        except:
            return self.send("msg", [user, text], reply_id=reply_id, enable_preview=enable_preview)

    #SENDING PHOTOS
    def send_photo(self, user, image, caption, reply_id=None):
        caption = '"{}"'.format(caption.replace("\\", "\\\\").replace("\"", "\\\"")).replace("\n", "\\n")
        try:
            temp = self.send("send_photo", ["{}#{}".format(user["type"], user["id"]), image, caption], reply_id=reply_id)
            # os.remove(image)
            return temp
        except:
            temp = self.send("send_photo", [user, image, caption], reply_id=reply_id)
            # os.remove(image)
            return temp

    #SENDING DOCUMENTS
    def send_document(self, user, file, reply_id=None):
        try:
            temp = self.send("send_document", ["{}#{}".format(user["type"], user["id"]), file], reply_id=reply_id)
            # os.remove(file)
            return temp
        except:
            temp = self.send("send_document", [user, file], reply_id=reply_id)
            # os.remove(file)
            return temp

    #SENDING
    def send(self, cli_command, args, reply_id=None, enable_preview=None):
        """
        This function will join the cli_command with the given parameters (dynamic *args),
        and execute _do_send(request,**kwargs)
        and execute _do_send(request,**kwargs)
        :keyword reply_id: The message id which this command is a reply to. (will be ignored by the CLI with non-sending commands)
        :type    reply_id: int or None
        :keyword enable_preview: If the URL found in a message should have a preview. (will be ignored by the CLI with non-sending commands)
        :type    enable_preview: bool
        :keyword retry_connect: How often the initial connection should be retried. default: 2. Negative number means infinite.
        :type    retry_connect: int
        """
        reply_part = ""
        if reply_id:
            if not isinstance(reply_id, int):
                raise AttributeError("reply_id keyword argument is not integer.")
            reply_part = "[reply=%i]" % reply_id
        preview_part = "[enable_preview]" if enable_preview else "[disable_preview]"
        arg_string = " ".join([u(x) for x in args])
        request = " ".join([reply_part, preview_part, cli_command, arg_string])
        request = "".join([request, "\n"])  # TODO can this be deleted?
        print(request)
        thread = threading.Thread(target=self.do, args=(request,))
        thread.daemon = True
        thread.start()
        # result = self.do(request)
        # return result

    #SENDING RAW
    def raw(self, cli_command, preview=False):
        request = u("{}\n".format(cli_command))
        print(request)
        thread = threading.Thread(target=self.do, args=(request,))
        thread.daemon = True
        thread.start()

    #TALKING TO TG-CLI
    def do(self, command, returnMessage=False):
        if not isinstance(command, (text_type, binary_type)):
            raise TypeError("Command to send is not a unicode(?) string. (Instead of %s you used %s.) " % (str(text_type), str(type(command))))

        if returnMessage:
            s = socket.socket()
            s.connect((self.host, self.port))
            try:
                s.sendall(b(command))
            except Exception as error:
                s.close()
                raise error
            s.close()
        else:
            try:
                if not self.s:
                    self.s = socket.socket()
                    self.s.connect((self.host, self.port))

                self.s.sendall(b(command))
            except Exception as error:
                try:
                    self.s.close()
                except:
                    pass
                self.s = None
                raise error

        if not returnMessage:
            return

        completed = -1
        buffer = b("")
        while completed != 0:
            try:
                while 1:
                    try:
                        answer = s.recv(1)
                        if len(answer) == 0:
                            raise ConnectionError("Remote end closed")
                        break
                    except Exception as err:
                        raise
                buffer += answer
                if completed < -1 and buffer[:len(self._ANSWER_SYNTAX)] != self._ANSWER_SYNTAX[:len(buffer)]:
                    raise ArithmeticError("Server response does not fit.")
                if completed <= -1 and buffer.startswith(self._ANSWER_SYNTAX) and buffer.endswith(self._LINE_BREAK):
                    completed = int(n(buffer[7:-1]))
                    buffer = b("")
                completed -= 1
            except ConnectionError:
                s.close()
                s = None
                raise
            except socket.timeout:
                raise NoResponse(command)
            except KeyboardInterrupt as error:
                s.close()
                s = None
                raise
            except Exception as error:
                s.close()
                s = None
                raise
        if s:
            s.close()
            s = None
        return u(buffer)


##Botato##

def blockCheck(msg, command=""):
    # if msg["to"].type == tgl.PEER_CHAT:
        # print(msg["to"].user_list)
    if msg["from"]["id"] in settingsData["blocked_users"]:
        return True
    elif (msg["to"]["type"] != "user") and (msg["to"]["id"] in settingsData["blocked_chats"]):
        return True
    else:
        return False


def loadPlugins():
    pluginFiles = [curPath + "/plugins/" + f for f in listdir(curPath + "/plugins") if re.search('^.+\.py$', f)]
    global plugins
    plugins = [adminPlugin, helpPlugin]
    for file in pluginFiles:
        values = {}
        with open(file) as f:
            code = compile(f.read(), file, 'exec')
            exec(code, values)
        plugin = values['plugin']
        print("Initializing plugin: {}".format(plugin['name']))
        plugins.append(plugin)

    def keyfunction(item):
        return item["tag"]

    plugins.sort(key=keyfunction)

######### ADMIN TOOLS #########


def adminFunction(sender, msg, matches, peer):
    global settingsData
    key = matches[0].lower()
    if key == "reboot" or key == "restart":
        loadPlugins()
        sender.send_msg(msg["from"], "Rebooted", reply_id=msg["id"])
    elif key == "ban":
        settingsData = settings.blockUser(int(matches[1]))
    elif key == "unban":
        settingsData = settings.unblockUser(int(matches[1]))
    elif key == "sudo":
        settingsData = settings.addSudo(int(matches[1]))
    elif key == "unsudo":
        settingsData = settings.removeSudo(int(matches[1]))
    elif key == "raw":
        print(matches[1])
        return sender.do("{}\n".format(matches[1]), True)
#    elif key == "list":
#        return get_groups

our_id = 103332821

adminPlugin = {
    'name': "Admin",
    'tag': "admin",
    'patterns': ["^/(?:admin)? ?(reboot|restart)$", 
                 "^/admin ((?:un)?(?:ban|sudo)) (\d+)$",
                 "^/(?:admin)? ?(raw) (.*?)$",
                 "^/admin (list) (groups)$"],
    'function': adminFunction,
    'elevated': True,
    'usage': "/admin [unban|ban|unsudo|sudo|raw]",
    'desc': "Admin tools"
    }

######### ADMIN TOOLS #########

############# HELP ############


def helpFunction(sender, msg, matches, peer):
    helpText = ""
    if len(matches[0]) > 1:
        for a in plugins:
            if matches[0] == a["tag"] and not a["elevated"]:
                return "{}{} : {}".format(e1, a["usage"], a["desc"])
    else:
        for a in plugins:
            if not a["elevated"] and a["usage"] and a["tag"]:
                helpText = helpText + e1 + a["usage"] + "\n"
    if msg["to"]["id"] != our_id:
        sender.send_msg(peer, "Help was sent in a pm", reply_id=msg["id"])
    sender.send_msg(msg["from"], helpText, reply_id=msg["id"])

helpPlugin = {
    'name': "Help",
    'tag': "help",
    'patterns': ["^/help$", "^/help (.+)$"],
    'function': helpFunction,
    'elevated': False,
    'usage': "/help [action]",
    'desc': "Display a message showing all commands or description of specific command"
    }

############# HELP ############

plugins = [adminPlugin, helpPlugin]


def rec(msg):
    if msg["to"]["id"] == our_id:
        return "{}#{}".format(msg["from"]["type"], msg["from"]["id"])
    else:
        return "{}#{}".format(msg["to"]["type"], msg["to"]["id"])


def on_msg_receive(sender, msg):
    # if msg["from"]["id"] != 82725741: return
    nowTime = time.time()
    peer = rec(msg)
    sender.raw("mark_read {}".format(peer))
    if msg["out"]:
        return
    if nowTime - msg["date"] > 10:
        return
    if blockCheck(msg):
        return
    if msg["text"].strip():
        for aPlugin in plugins:
            for aPattern in aPlugin['patterns']:
                if re.search(aPattern, msg["text"], re.IGNORECASE) and not blockCheck(msg, aPlugin["tag"]):
                    print("Found a command and will send message now")
                    matches = re.search(aPattern, msg["text"], re.IGNORECASE)
                    if matches.groups():
                        matches = matches.groups()
                    else:
                        matches = matches.group()
                    if aPlugin['elevated']:
                        if msg["from"]["id"] in admins:
                            someReturnValue = aPlugin['function'](sender, msg, matches, peer)
                            if someReturnValue:
                                sender.send_msg(peer, someReturnValue, reply_id=msg["id"])
                        else:
                            sender.send_msg(peer, 'The function requires an elevated user.', reply_id=msg["id"])
                    else:
                        someReturnValue = aPlugin['function'](sender, msg, matches, peer)
                        if someReturnValue:
                            sender.send_msg(peer, someReturnValue, reply_id=msg["id"])
                    break

sender = Sender()
e1 = b'\xf0\x9f\x94\xb8'.decode("utf-8")
settingsData = settings.readSettings()
curPath = dirname(realpath(__file__))
admins = [82725741]

try:
    loadPlugins()
    popen = subprocess.Popen(['/home/juan/tg/telegram-cli', '-R', '-C', '-k', '/home/juan/tg/tg-server.pub', '--json', '-P', '4458'], stdout=subprocess.PIPE)
    lines_iterator = iter(popen.stdout.readline, b"")
    for line in lines_iterator:
        msg = line.decode("utf-8").strip()
        mat = re.search("{[\\s\\S]*}", msg)
        if mat:
            print(mat.group())
            j = json.loads(mat.group())
            if "text" in j:
                on_msg_receive(sender, j)
except:
    raise
    # if sender:
        # sender.do("safe_quit")
