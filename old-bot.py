import subprocess
import socket
import json
import re
from encodingUtil import to_unicode as u
from encodingUtil import to_binary as b
from encodingUtil import to_native as n
from encodingUtil import text_type, binary_type

# class Sender:
	# _ANSWER_SYNTAX = b"ANSWER "
	# _LINE_BREAK = b"\n"
	# s = None

	# def send(self, command, answer_timeout=3.0):
		# if self.s:
			# self.s.close()
			# self.s = None
		# self.s = socket.socket()
		# try:
			# self.s.connect(('', 4458))
		# except Exception as error:
			# self.s.close()
			# raise ConnectionError("Could not establish connection to the cli port.")
			# raise error
		# except Exception as error:
			# self.s.close()
			# raise error
		# try:
			# self.s.sendall(str.encode(command))
		# except Exception as error:
			# self.s.close()
			# raise error
		# completed = -1
		# buffer = b""
		# self.s.settimeout(answer_timeout)
		# while completed != 0:
			# try:
				# while 1:
					# try:
						# answer = self.s.recv(1)
						# if len(answer) == 0:
							# raise ConnectionError("Remote end closed")
						# break
					# except socket_error as err:
						# raise
				# self.s.settimeout(max(1.0, answer_timeout))
				# buffer += answer
				# if completed < -1 and buffer[:len(self._ANSWER_SYNTAX)] != self._ANSWER_SYNTAX[:len(buffer)]:
					# raise ArithmeticError("Server response does not fit.")
				# if completed <= -1 and buffer.startswith(self._ANSWER_SYNTAX) and buffer.endswith(self._LINE_BREAK):
					# completed = int(n(buffer[7:-1]))
					# buffer = b""
				# completed -= 1
			# except ConnectionError:
				# self.s.close()
				# raise
			# except socket.timeout:
				# raise NoResponse(command)
			# except KeyboardInterrupt as error:
				# logger.exception("Exception while reading the Answer for \"%s\". Got so far: >%s< of %i\n" % (n(command), n(buffer), completed))
				# self.s.close()
				# raise
			# except Exception as error:
				# logger.exception("Exception while reading the Answer for \"%s\". Got so far: >%s<\n" % (n(command), n(buffer)))
				# self.s.close()
				# raise
		# if self.s:
			# self.s.close()
			# self.s = None
		# return buffer.decode("utf-8")
		
class Sender():
	_ANSWER_SYNTAX = b("ANSWER ")
	_LINE_BREAK = b("\n")
	default_answer_timeout = 1.0 # how long it should wait for a answer. DANGER: if set to None it will block!

	def __init__(self, host='', port=4458):
		self.s = None
		self.host = host
		self.port = port

	def send(self, cli_command, args, reply_id=None, enable_preview=None):
		"""
		This function will join the cli_command with the given parameters (dynamic *args),
		and execute _do_send(request,**kwargs)
		:keyword reply_id: The message id which this command is a reply to. (will be ignored by the CLI with non-sending commands)
		:type    reply_id: int or None
		:keyword enable_preview: If the URL found in a message should have a preview. (will be ignored by the CLI with non-sending commands)
		:type    enable_preview: bool
		:keyword retry_connect: How often the initial connection should be retried. default: 2. Negative number means infinite.
		:type    retry_connect: int
		"""
		reply_part = ""
		if reply_id:
			if not isinstance(reply_id, int):
				raise AttributeError("reply_id keyword argument is not integer.")
			reply_part = "[reply=%i]" % reply_id
		preview_part = "[enable_preview]" if enable_preview else "[disable_preview]"
		arg_string = " ".join([u(x) for x in args])
		request = " ".join([reply_part, preview_part, cli_command,  arg_string])
		request = "".join([request, "\n"]) #TODO can this be deleted?
		result = self._do_send(request)
		return result

	def _do_send(self, command):
		"""
		You can force retry with retry_connect=2 (3 tries, default settings, first try + 2 retries)
		retry_connect=0 , retry_connect=False and retry_connect=None  means not to retry,
		retry_connect=True or retry_connect= -1 means to retry infinite times.
		:type command: builtins.str
		:type answer_timeout: builtins.float or builtins.int
		:param retry_connect: How often the initial connection should be retried. default: 2. Negative number means infinite.
		:type  retry_connect: int
		:return:
		"""

		if not isinstance(command, (text_type, binary_type)):
			raise TypeError("Command to send is not a unicode(?) string. (Instead of %s you used %s.) " % (str(text_type), str(type(command))))
		# logger.debug("Sending command >%s<" % n(command))
		if self.s:
			self.s.close()
			self.s = None
		self.s = socket.socket()
		try:
			self.s.connect((self.host, self.port))
		except Exception as error:
			self.s.close()
			raise error
		# logger.debug("Socket Connected.")
		try:
			self.s.sendall(b(command))
		except Exception as error:
			self.s.close()
			raise error #retry?
		# logger.debug("All Sent.")
		completed = -1 # -1 = answer size yet unknown, >0 = got remaining answer size
		buffer = b("")
		while completed != 0:
			try:
				while 1: #retry if CTRL+C'd
					try:
						answer = self.s.recv(1)
						# recv() returns an empty string if the remote end is closed
						if len(answer) == 0:
							raise ConnectionError("Remote end closed")
						break
					except Exception as err:
						raise
						# logger.exception("Uncatched exception in reading answer from cli.")
				# If there was input the input is now either the default one or the given one, which waits longer.
				buffer += answer
				if completed < -1 and buffer[:len(self._ANSWER_SYNTAX)] != self._ANSWER_SYNTAX[:len(buffer)]:
					raise ArithmeticError("Server response does not fit.")
				if completed <= -1 and buffer.startswith(self._ANSWER_SYNTAX) and buffer.endswith(self._LINE_BREAK):
					completed = int(n(buffer[7:-1])) #TODO regex.
					buffer = b("")
				completed -= 1
			except ConnectionError:
				self.s.close()
				raise
			except socket.timeout:
				raise NoResponse(command)
			except KeyboardInterrupt as error:
				# logger.exception("Exception while reading the Answer for \"%s\". Got so far: >%s< of %i\n" % (n(command), n(buffer), completed))  # TODO remove me
				self.s.close()
				raise
			except Exception as error:
				# logger.exception("Exception while reading the Answer for \"%s\". Got so far: >%s<\n" % (n(command), n(buffer))) #TODO remove me
				self.s.close()
				raise
				#raise error
		# end while completed != 0
		if self.s:
			self.s.close()
			self.s = None
		return u(buffer)
			# end while not self._do_quit
		# end with lock
		if self.s:
			self.s.close()
	# end of function


# proc = subprocess.Popen(['/home/juan/tg/telegram-cli', '-k', '/home/juan/tg/tg-server.pub', '--json', '-p', '4458'],stdout=subprocess.PIPE)
sender = Sender()

# while True:
	# line = str(proc.stdout.readline().strip()).decode("utf-8")
	# if len(line) > 5:
		# print(line)
	# try:
		# if line[0] == "{":
			# j = json.loads(line)
			# print("log1")
			# if "text" in j:
				# print("log2")
				# if j["text"] == "/ping4458":
					# print("log3")
					# a = sender.send("msg {}#{} ping #4458".format(j["from"]["type"], j["from"]["id"]))
		# else:
			# print(line[0].encode("hex"))
	# except:
		# raise

sender = Sender()
popen = subprocess.Popen(['/home/juan/tg/telegram-cli', '-k', '/home/juan/tg/tg-server.pub', '--json', '-P', '4458'], stdout=subprocess.PIPE)
lines_iterator = iter(popen.stdout.readline, b"")
for line in lines_iterator:
	msg = line.decode("utf-8").strip()
	mat = re.search("{[\\s\\S]*}", msg)
	if mat:
		print(mat.group())
		j = json.loads(mat.group())
		if "text" in j:
			if j["text"] == "/ping4458":
				print("DING DING DING")
				a = sender.send("msg", ["{}#{}".format(j["from"]["type"], j["from"]["id"]), "ping:4458"])
